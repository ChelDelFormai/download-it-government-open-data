const axios = require('axios')
const https = require('https')
const fs = require('fs')
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

/**
*  ===== Variables =====
*/

const data_dir = "data"
let keyword_to_search = ""

/**
*  ===== Functions =====
*/

// create the directory to store the data into if it doensn't already exist
const create_directory = function (data_dir) {
    try {
        // first check if directory already exists
        if (!fs.existsSync(data_dir)) {
            fs.mkdirSync(data_dir)
            console.log("Directory 'data' has been created.")
        } else {
            console.log("Directory 'data' already exists.")
        }
    } catch (err) {
        console.error(err)
    }
}

// returns an array with all the ids that include the keyword defined in the variable "keyword_to_search" in their names, stupid statal anti patterns
const get_ids_by_keyword = async function (keyword) {

    let ids_with_keyword = []

    try {
        const all_data_from_ids = await axios.get('https://www.dati.gov.it/opendata/api/3/action/package_list')
        ids_with_keyword = all_data_from_ids.data.result.filter(id => {
            return id.includes(keyword)
        })
    } catch (err) {
        console.error(err)
        throw err
    }

    return ids_with_keyword
}

// returns an array of objects with all the data of the passed ids and the id itself for every object
const get_info_for_all_ids = async function (ids) {
    const promises = ids.map(async id => {
        return {
            id: id,
            data: (await axios.get(`https://www.dati.gov.it/opendata/api/3/action/package_show?id=${id}`)).data.result
        }
    })
    const data = await Promise.all(promises)
    return data 
}

/*
* download files from the infos, infos are structured like this:
* info = {
*    id: string
*    data: {
*       id: string
*       dataset: string
*       resources: {
*           description: string
*           format: string
*           access_url: string
*       }
*   }
* }
*
* * TODO: fix the downloads via https to resolve the promises when ALL downloads have finished
*
*/
const dowload_files_from_infos = async function (infos) {
    const promises = infos.map(async info => {
        info.data.resources.forEach(resource => {
            https.get(resource.access_url,(res) => {
                // the file will be saved in this path, the name will be like: 
                // id of info-id of the resource.format
                const path = `${__dirname}/${data_dir}/${info.id + "-" + info.data.id }.${resource.format}`
                const filePath = fs.createWriteStream(path)
                res.pipe(filePath)
                console.log(`Downloading ${info.id + "-" + info.data.id }.${resource.format}...`); 
                filePath.on('finish',() => {
                    // close the write stream
                    filePath.close()
                    console.log('Download Completed')
                })
            })
        })

    return await Promise.all(promises)
    
    })
}

// initializes a promt and waits for the user's input, returns a promise that resolves when ENTER is pressed
const enter_keyword = async function () {      
    
    return new Promise((resolve,reject) => {
        readline.question(`Enter the keyword to search (blank for the entire database): `, keyword => {
            if(keyword != "")
                console.log(`Starting the search for the keyword "${keyword}"...`)
            else
                console.log(`Starting to download the entire database, this might take a while...`)
            readline.close()
            resolve(keyword)
        })
    })
}

/**
 *  ===== Execution ======
*/

/**
 *  ===== Credits =====
 */

console.log("===== Created by Andrea Mazzoletti =====")

/**
*  ===== Workflow =====
*/

// create the directory to store the data
create_directory(data_dir)
// wait for the user to type the search keyword 
enter_keyword().then(keyword => {
    keyword_to_search = keyword
    // get all ids that include the passed keyword stored in "keyword_to_search" in their names
    get_ids_by_keyword(keyword_to_search).then( ids => {
        if(ids.length <= 0)
            console.log(`No data entries were found for "${keyword_to_search}".`)
        else 
            console.log(`${ids.length} unique entries found. Proceeding with the download...`)
        // get informations about the obtained ids including the download link
        get_info_for_all_ids(ids).then( ids_infos => {
            // download all the resources for every information block obtained (mainly they wil be CSV, JSON, SHP, TSV AND XML)
            dowload_files_from_infos(ids_infos).then(() => {
                console.log("===== End of program workflow =====")
            })
        })
    })
})